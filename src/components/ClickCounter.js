import { Component } from "react";

class ClickCounter extends Component {

  state = { count: 0 }

  handleClick() {
    this.setState({ count: this.state.count + 1 })
  }

  render() {
    return (
      <div>
        <h1>{this.state.count}</h1>
        <button onClick={this.handleClick.bind(this)}>Click</button>
      </div>
    );
  }

}

export default ClickCounter;