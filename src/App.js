import './App.css';

import ClickCounter from './components/ClickCounter';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ClickCounter></ClickCounter>
      </header>
    </div>
  );
}

export default App;
